## Edit Content Type Tab

Edit Content Type Tab (ECTT) is designed to allow quick access to
the content type editing form of the current node.

This is very handy when templating/site building especially if
there are many content types.

ECTT adds a tab into the node view and edit form which will link
to the associated content type (for users with administer content
types permissions).

### Requirements

No special requirements at this time.

### Installation

Install module via composer:

```bash
composer require drupal/edit_content_type_tab
```

Enable module via drush:

```bash
drush en edit_content_type_tab
```

### Maintainers

* George Anderson (geoanders) - https://www.drupal.org/u/geoanders
* Ian Flory - (http://drupal.org/user/1224098)
