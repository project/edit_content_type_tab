<?php

namespace Drupal\edit_content_type_tab;

use Symfony\Component\HttpFoundation\Request;

/**
 * A request service that can accept different types of request object.
 *
 * Used so we are not limited to HTTP requests only.
 */
class RequestService {

  /**
   * The request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Request setter.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   */
  public function setRequest(Request $request) {
    $this->request = $request;
  }

  /**
   * Request getter.
   *
   * @return \Symfony\Component\HttpFoundation\Request
   *   Returns the request.
   */
  public function getRequest() {
    return $this->request;
  }

}
